<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData= $this->request->getData();
            $formData['winner_dresseur_id']= $this->_computerFight($formData);
        $fight = $this->Fights->patchEntity($fight,$formData);
            if ($this->Fights->save($fight)) {
                $this->Flash->success(__('The fight has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fight could not be saved. Please, try again.'));
        }
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'firstDresseurs', 'secondDresseurs'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



    protected function getPokes($id_trainer)
    {
        $output = array();
        $output_id = array();
        $outputtemp = array();
        $compt = 0;
        $query = TableRegistry::getTableLocator()->get('DresseursPokemons')->find()->select(['poke_id', 'is_fav'])->where(['dresseur_id =' => $id_trainer]);
        foreach ($query as $data) {
            if ($data['is_fav'] == true)
            {
                $outputtemp = array();
                $outputtemp[0] = $data['poke_id'];
                for ($i = 1; $i <= count($output_id); $i++)
                {
                    $outputtemp[$i] = $output_id[$i - 1];
                }
                $output_id = $outputtemp;
            }
            else
            {
                $output_id[$compt] = $data['poke_id'];
            }
            $compt++;
        }
        for ($i = 0; $i < count($output_id); $i++)
        {
            $query = TableRegistry::getTableLocator()->get('Pokes')->find()->select(['name', 'attack', 'health', 'defense'])->where(['id =' => $output_id[$i]]);
            foreach ($query as $data){
                $poke = [
                    "name" => $data['name'],
                    "HP" => $data['health'],
                    "ATT" => $data['attack'],
                    "DEF" => $data['defense'],
                ];
                $output[$i] = $poke;
            }
        }
        return $output;
    }

    protected function damage($attacker, $defenser)
    {
        $output = array();
        if($defenser['DEF'] > 0)
        {
            $output['dgt'] = ($attacker['ATT'] / $defenser['DEF'] + 2);
        }
        else
        {
            $output['dgt'] = $attacker['ATT'];
        }
        return $output;
    }

    protected function attack($first, $second)
    {
        $out = array();
        $out['res'] = 0;

        $out['log'] = $first['name']." attaque ".$second['name'].".\n";
        $att = $this->damage($first, $second);
        $second['HP'] -= $att['dgt'];
        $out['log'] = $out['log'].$att['log'];

        if($second['HP'] <= 0)
        {
            $out['log'] = $out['log'].$second['name']." est K.O.\n";
            $out['res']++;
        }
        else
        {
            $out['log'] = $out['log'].$second['name']." attaque ".$first['name'].".\n";
            $att = $this->damage($second, $first);
            $first['HP'] -= $att['dgt'];
            $out['log'] = $out['log'].$att['log'];

            if($first['HP'] <= 0)
            {
                $out['log'] = $out['log'].$first['name']." est K.O.\n";
                $out['res']++;
            }
        }
        $out['first'] = $first;
        $out['second'] = $second;

        return $out;
    }


    protected function _computerFight($data)
    {
            /*$pvpkm1=100;
            $pvpkm2=100;
            while ($pvpkm1 > 0 && $pvpkm2 > 0 )
            {
                $pvpkm1=$pvpkm1-$poke->attack;
                $pvpkm2=$pvpkm2-rand(5,15);
            }
            
            if($pvpkm1 > $pvpkm2){
                return $firstDresseur['DresseurPokes'];
            }
            if($pvpkm1 < $pvpkm2){
                return $secDresseur['DresseurPokes'];
            }
            if($pvpkm1 == $pvpkm2){
                return $secDresseur['DresseurPokes'];
            }
            */
            $pokemon1=getPokes(1);
            $pokemon2=getPokes(2);
            while($pokemon1['HP'] > 0 && $pokemon2['HP'] > 0)
            {
                attack($pokemon1,$pokemon2)
            }
            if($pokemon1['HP'] > $pokemon2['HP']){
                return $firstDresseur['first_dresseur_id'];
            }
            if($pokemon1['HP'] < $pokemon2['HP']){
                return $secDresseur['second_dresseur_id'];
            }
            if($pokemon1['HP'] == $pokemon2['HP']){
                return $secDresseur['second_dresseur_id'];
            }

    }
}
