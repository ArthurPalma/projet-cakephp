<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur[]|\Cake\Collection\CollectionInterface $dresseurs
 */
?>
<style>
.titre {
    text-align: center;
    font-style: bold;
    font-size: 40px;
}
</style>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?></li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="dresseurs index large-9 medium-8 columns content">
    <h3 class="titre"><?= __('Dresseurs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nom') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prenom') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurs as $dresseur): ?>
            <tr>
                <td><?= $this->Number->format($dresseur->id) ?></td>
                <td><?= h($dresseur->nom) ?></td>
                <td><?= h($dresseur->prenom) ?></td>
                <td><?= h($dresseur->created) ?></td>
                <td><?= h($dresseur->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseur->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseur->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
