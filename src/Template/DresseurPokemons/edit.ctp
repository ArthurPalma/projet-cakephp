<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseurPokemon $dresseurPokemon
 */
?>

<style>
fieldset {
    font-family: monospace; /* 1 */
    -ms-text-size-adjust: 100%; /* 2 */
    -webkit-text-size-adjust: 100%; /* 2 */
    font-size: 20px;
    color: red;
}
</style>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dresseurPokemon->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemon->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseurs'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Poke'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="dresseurPokemons form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseurPokemon) ?>
    <fieldset class="fieldset">
        <legend><?= __('Edit Dresseur Pokemon') ?></legend>
        <?php
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('poke_id', ['options' => $pokes]);
            echo $this->Form->control('favoris');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
