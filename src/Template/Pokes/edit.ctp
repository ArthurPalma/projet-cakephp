<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $poke->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?></li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>
<div class="pokes form large-9 medium-8 columns content">
    <?= $this->Form->create($poke) ?>
    <fieldset>
        <legend><?= __('Edit Poke') ?></legend>
        <?php
            echo $this->Form->control('nom');
            echo $this->Form->control('pokedex_number');
            echo $this->Form->control('health');
            echo $this->Form->control('attack');
            echo $this->Form->control('defense');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
