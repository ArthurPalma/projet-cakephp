<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<style>
.row{
    font-family: monospace; /* 1 */
    -ms-text-size-adjust: 100%; /* 2 */
    -webkit-text-size-adjust: 100%; /* 2 */
    font-size: 15px;
}

.panel-title{
    font-family: monospace; /* 1 */
    -ms-text-size-adjust: 100%; /* 2 */
    -webkit-text-size-adjust: 100%; /* 2 */
    font-size: 25px;
    color: #92a8d1; 
}


</style>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poke'), ['action' => 'edit', $poke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poke'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pokes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poke'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Dresseur Pokemons'), ['controller' => 'DresseurPokemons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Dresseur Pokemon'), ['controller' => 'DresseurPokemons', 'action' => 'add']) ?> </li>
        <li><a href="http://localhost/cakephp/Dresseurs">Dresseurs</a></li>
        <li><a href="http://localhost/cakephp/DresseurPokemons">DresseurPokemons</a></li>
        <li><a href="http://localhost/cakephp/Pokes">Pokes</a></li>
        <li><a href="http://localhost/cakephp/Fights">Fights</a></li>
    </ul>
</nav>



<div class="pokes view large-9 medium-12 columns content">
   
   
   <div class="panel font">
     <div class="panel-heading">
       <h3 class="panel-title"><?= h($poke->nom) ?></h3>
     </div>
     <div class="panel-body">
       <div class="row">
       <?php if ($poke->nom == "pikachu") { ?>
         <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://www.pokepedia.fr/images/thumb/e/e7/Pikachu-RFVF.png/640px-Pikachu-RFVF.png" class="img-circle img-responsive"> </div>
         <?php } elseif ($poke->nom == "salameche") { ?>
       <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="https://www.pokepedia.fr/images/thumb/8/89/Salam%C3%A8che-RFVF.png/530px-Salam%C3%A8che-RFVF.png" class="img-circle img-responsive"> </div>
       <?php }?>
         <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
           <dl>
             <dt>DEPARTMENT:</dt>
             <dd>Administrator</dd>
             <dt>HIRE DATE</dt>
             <dd>11/12/2013</dd>
             <dt>DATE OF BIRTH</dt>
                <dd>11/12/2013</dd>
             <dt>GENDER</dt>
             <dd>Male</dd>
           </dl>
         </div>-->
         <div class=" col-md-18 col-lg-9 "> 
           <table class="table table-user-information">
             <tbody>
               <tr>
               <th scope="row" class="name"><?= __('Nom') ?></th>
               <td><?= h($poke->nom) ?></td>
               </tr>
               <tr>
               <th scope="row"><?= __('Id') ?></th>
               <td><?= $this->Number->format($poke->id) ?></td>
               </tr>
               <tr>
               <th scope="row"><?= __('Pokedex Number') ?></th>
               <td><?= $this->Number->format($poke->pokedex_number) ?></td>
               </tr>
            
                  <tr>
                      <tr>
                      <th scope="row"><?= __('Health') ?></th>
                      <td><?= $this->Number->format($poke->health) ?></td>
               </tr>
                 <tr>
                 <th scope="row"><?= __('Attack') ?></th>
                 <td><?= $this->Number->format($poke->attack) ?></td>
               </tr>
               <tr>
               <th scope="row"><?= __('Defense') ?></th>
               <td><?= $this->Number->format($poke->defense) ?></td>
               </tr>
               <th scope="row"><?= __('Created') ?></th>
                <td><?= h($poke->created) ?></td>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($poke->modified) ?></td>
                 </td>
                    
               </tr>
              
             </tbody>
           </table>
           
         </div>
       </div>
     </div>
     
   </div>
 </div>
</div>
<div class="pokes view large-9 medium-8 columns content">
<div>
<div>
    <div class="related">
        <h4><?= __('Related Dresseur Pokemons') ?></h4>
        <?php if (!empty($poke->dresseur_pokemons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Dresseur Id') ?></th>
                <th scope="col"><?= __('Poke Id') ?></th>
                <th scope="col"><?= __('Favoris') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Is Fav') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poke->dresseur_pokemons as $dresseurPokemons): ?>
            <tr>
                <td><?= h($dresseurPokemons->id) ?></td>
                <td><?= h($dresseurPokemons->dresseur_id) ?></td>
                <td><?= h($dresseurPokemons->poke_id) ?></td>
                <td><?= h($dresseurPokemons->favoris) ?></td>
                <td><?= h($dresseurPokemons->created) ?></td>
                <td><?= h($dresseurPokemons->modified) ?></td>
                <td><?= h($dresseurPokemons->is_fav) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DresseurPokemons', 'action' => 'view', $dresseurPokemons->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DresseurPokemons', 'action' => 'edit', $dresseurPokemons->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DresseurPokemons', 'action' => 'delete', $dresseurPokemons->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurPokemons->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>



        <?php endif; ?>
    </div>
</div>
</div>
</div>

