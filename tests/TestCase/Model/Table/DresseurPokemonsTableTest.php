<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DresseurPokemonsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DresseurPokemonsTable Test Case
 */
class DresseurPokemonsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DresseurPokemonsTable
     */
    public $DresseurPokemons;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.DresseurPokemons',
        'app.Dresseurs',
        'app.Pokes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DresseurPokemons') ? [] : ['className' => DresseurPokemonsTable::class];
        $this->DresseurPokemons = TableRegistry::getTableLocator()->get('DresseurPokemons', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DresseurPokemons);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
